// new Date(year, month, day, hours, minutes, seconds, milliseconds);

var data = [
    {   
        "start":new Date(2013, 11,9, 0, 0, 0, 0),
        "end":new Date(2013, 11,30, 0, 0, 0, 0),
        "name":"Fas 1",
        "sections":[{"name":'del 1.1',"duration":40}]
    }
];
// console.log(data);
// var tasks = [
//     {   
//         "startDate":new Date("Sun Dec 09 01:36:45 EST 2012"),
//         "endDate":new Date("Sun Dec 09 02:36:45 EST 2012"),
//         "phaseName":"E Job",
//         "status":"RUNNING"
//     }

// ];

// var taskStatus = {
//     "SUCCEEDED" : "bar",
//     "FAILED" : "bar-failed",
//     "RUNNING" : "bar-running",
//     "KILLED" : "bar-killed"
// };

// var taskNames = [ "D Job", "P Job", "E Job", "A Job", "N Job" ];

// tasks.sort(function(a, b) {
//     return a.endDate - b.endDate;
// });
// var maxDate = tasks[tasks.length - 1].endDate;
// tasks.sort(function(a, b) {
//     return a.startDate - b.startDate;
// });
// var minDate = tasks[0].startDate;

var format = "%a %e/%m";
var format = "%b %d";
var timeDomainString = "1day";

// var gantt = d3.gantt().height(200).tickFormat(format);

// gantt.timeDomainMode("fit");

// data.push({   
//     "start":new Date(2013, 11,10, 0, 0, 0, 0),
//     "end":new Date(2014, 0,22, 0, 0, 0, 0),
//     "name":"Projektfas",
//     "sections":[{"name":'del 1',"duration":56},{"name":'del 2',"duration":56}]
// });

// gantt();
// gantt.data(data);
// gantt.redraw();

function changeTimeDomain(timeDomainString) {
    this.timeDomainString = timeDomainString;
    switch (timeDomainString) {
    case "1hr":
	format = "%H:%M:%S";
	gantt.timeDomain([ d3.time.hour.offset(getEndDate(), -1), getEndDate() ]);
	break;
    case "3hr":
	format = "%H:%M";
	gantt.timeDomain([ d3.time.hour.offset(getEndDate(), -3), getEndDate() ]);
	break;

    case "6hr":
	format = "%H:%M";
	gantt.timeDomain([ d3.time.hour.offset(getEndDate(), -6), getEndDate() ]);
	break;

    case "1day":
	format = "%H:%M";
	gantt.timeDomain([ d3.time.day.offset(getEndDate(), -1), getEndDate() ]);
	break;

    case "1week":
	format = "%a %H:%M";
	gantt.timeDomain([ d3.time.day.offset(getEndDate(), -7), getEndDate() ]);
	break;
    default:
	format = "%H:%M"

    }
    gantt.tickFormat(format);
    gantt.redraw(tasks);
}

function addTask() {

    data.push({   
        "start":new Date(2013, Math.round(Math.random()*10),Math.round(Math.random()*30), 0, 0, 0, 0),
        "end":new Date(2013, 11,Math.round(Math.random()*30), 0, 0, 0, 0),
        "name":"Projektfas "+Math.round(Math.random()*5),
        "sections":[{"name":'del 1',"duration":56},{"name":'del 2',"duration":56}]
    });
    // console.log(data);
    gantt.data(data);
    gantt.redraw();
};

function removeTask() {
    data.pop();
    gantt.redraw();
};
function logData() {
    console.log(data);
    gantt.redraw();

};


function Planner($scope){
    $scope.gantt = d3.gantt().tickFormat(format).width($('#gantt').width());
    $scope.workDensity = d3.workDensity().height(80).tickFormat(format).width($('#gantt').width());

    var startdate = new Date();
    var enddate = d3.time.day.offset(new Date(), 5);

    $scope.data = [
        {   
            "start":new Date(),
            "end":d3.time.day.offset(new Date(), 5),
            "name":"Analys",
            "sections":[{"name":'Uppstart',"duration":8,"description":'beskrivning'},{"name":'Datainsamling',"duration":8,"description":'beskrivning'}],
            "description":'beskrivning'
        },
        {   
            "start":d3.time.day.offset(new Date(), 5),
            "end":d3.time.day.offset(new Date(), 10),
            "name":"Koncept",
            "sections":[{"name":'Boost',"duration":10,"description":'beskrivning'},{"name":'Koncept',"duration":16,"description":'beskrivning'},{"name":'Sammanställning',"duration":8,"description":'beskrivning'}],
            "description":'beskrivning'
        },
        {   
            "start":d3.time.day.offset(new Date(), 10),
            "end":d3.time.day.offset(new Date(), 15),
            "name":"Förädling",
            "sections":[{"name":'',"duration":16,"description":'beskrivning'}],
            "description":'beskrivning'
        }
        ,
        {   
            "start":d3.time.day.offset(new Date(), 15),
            "end":d3.time.day.offset(new Date(), 20),
            "name":"Leverans",
            "sections":[{"name":'',"duration":16,"description":'beskrivning'}],
            "description":'beskrivning'
        },
        {   
            "start":d3.time.day.offset(new Date(), 0),
            "end":d3.time.day.offset(new Date(), 25),
            "name":"Admininstration",
            "sections":[{"name":'',"duration":16,"description":'beskrivning'}],
            "description":'beskrivning'
        }
    ];
    calcSectionSum = function(input){
        
        for(i in input){
            // console.log(input[i])
            input[i].sections.sectionSum = d3.sum(input[i].sections.map(function(section){return section.duration;}));
        }
        // console.log(input);
    };
    
    calcTotalDateSpan = function(start,end) {
        if(start == undefined){
            timeDomainStart = d3.min($scope.data.map(function(d){return d.start}))
        }else{
            timeDomainStart = start;
        };
        if(end == undefined){
            timeDomainEnd = d3.max($scope.data.map(function(d){return d.end;}));
        }else{
            timeDomainEnd = end;
        }
        
        
        var oneDay = 24*60*60*1000; // hours*minutes*seconds*millisecond
        var diffDays = Math.round(Math.abs((timeDomainStart.getTime() - timeDomainEnd.getTime())/(oneDay)));

        return diffDays;
    }

    calcWorkDensity = function(input){
        var timeEnd = d3.max(input.map(function(d){return d.end;}));
        var timeStart = d3.min(input.map(function(d){return d.start}));
        var maxDens = 0;

        function getDates( d1, d2 ){
          var oneDay = 24*3600*1000;
          for (var d=[],ms=d1*1,last=d2*1;ms<=last;ms+=oneDay){
            d.push( {date:new Date(ms),value:0 });
          }
          return d;
        }

        var wdens = getDates(timeStart,timeEnd);

        for(x in wdens){
            sum = 0;
            for(y in input){
                
                if(input[y].end >= wdens[x].date && wdens[x].date >= input[y].start){
                            // console.log(calcTotalDateSpan(input[y].start,input[y].end));
                            // sum+= data[j].sections.sectionSum/calculateDatespan(data[j].start,data[j].end);
                            sum+= input[y].sections.sectionSum/calcTotalDateSpan(input[y].start,input[y].end);


                }
            }
            if(maxDens<sum){
                 maxDens = sum;
            }

            wdens[x].value = sum;
        }
       $scope.workDensity.graphData(wdens).maxDensity(maxDens);
    }
    
    calcSectionSum($scope.data);
    calcWorkDensity($scope.data);

    $scope.workDensity.data($scope.data);
    $scope.workDensity();
    $scope.workDensity.redraw();

    $scope.gantt();
    $scope.gantt.data($scope.data);
    // $scope.gantt.timeDomainMode("fixed");
    $scope.gantt.redraw();
    
    $scope.hourlyRate = 1000;
    $scope.buffer = 0.3;

    $scope.tempSection = {"name":'x',"duration":'0'};
    $scope.tempPhase = null;
    // $scope.$watchCollection('data', function(newNames, oldNames) {
    //  console.log('change');
    // });
    

    $scope.$watch('data', function(data) {
        calcSectionSum(data);
        calcWorkDensity(data);
        $scope.workDensity.redraw();
    },true);

    $scope.deletePhase = function(phase) {
        index = $scope.data.indexOf(phase);
         $scope.data.splice(index,1);
          $scope.gantt.redraw();

    }
    $scope.deleteSection = function(phase,section) {

        index = phase.sections.indexOf(section);
        console.log(phase,section,index);
        phase.sections.splice(index,1);
        console.log(phase.sections.length);
        if(phase.sections.length == 0) $scope.deletePhase(phase);
        $scope.gantt.redraw();

    }
    $scope.sumHours = function() {
        var sum = 0;

        for(x in $scope.data){
           sum += $scope.data[x].sections.sectionSum;
           // console.log($scope.data[x].sections.sectionSum);
        }
        return sum;
    }
    $scope.calcTotalDateSpan = function(start,end) {
        if(start == undefined){
            timeDomainStart = d3.min($scope.data.map(function(d){return d.start}))
        }else{
            timeDomainStart = start;
        };
        if(end == undefined){
            timeDomainEnd = d3.max($scope.data.map(function(d){return d.end;}));
        }else{
            timeDomainEnd = end;
        }
        
        
        var oneDay = 24*60*60*1000; // hours*minutes*seconds*millisecond
        var diffDays = Math.round(Math.abs((timeDomainStart.getTime() - timeDomainEnd.getTime())/(oneDay)));

        return diffDays;
    }
    $scope.addPhase = function() {
        var name = prompt('Namn');
        $scope.data.push({   
            "start":d3.time.day.offset(new Date(), 25),
            "end":d3.time.day.offset(new Date(), 30),
            "name":name,
            "sections":[{"name":'del 1',"duration":8},{"name":'del 2',"duration":8}]
        });
        // console.log(data);
        // $scope.gantt.data($scope.data);
        $scope.gantt.redraw();
    };
    
    $scope.editSection = function(phase,section,add) {       
        $scope.tempPhase = phase;
        $scope.tempSection = section;
        $scope.add = add;
        $scope.$apply();
        $('#editSection').modal()
    }
    $scope.editPhase = function(phase,add) {
        console.log(phase);       
        $scope.tempPhase = phase;
        $scope.add = add;
        $scope.$apply();
        $('#editPhase').modal()
    }
    $scope.saveSection = function() {
        if($scope.add){
            $scope.tempPhase.sections.push($scope.tempSection);
        }
        
        $('#editSection').modal('hide')
        $scope.refreshGraphs();
        console.log('saved');
    }

    $scope.savePhase = function() {
        $('#editPhase').modal('hide')
        $scope.refreshGraphs();
;
    }

    $scope.refreshGraphs = function() {
        $scope.workDensity.redraw();
        $scope.gantt.redraw();
        console.log('refresh');
    };
    // $scope.data = data;
    // console.log($scope.data);
}
