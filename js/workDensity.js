/**
 * @author Mikael Sundgren
 * @version 0.6
 */

d3.workDensity = function() {
    var FIT_TIME_DOMAIN_MODE = "fit";
    var FIXED_TIME_DOMAIN_MODE = "fixed";
    
    var margin = {
	top : 20,
	right : 40,
	bottom : 40,
	left : 0
    };

    var timeDomainStart = d3.time.day.offset(new Date(),-3);
    var timeDomainEnd = d3.time.hour.offset(new Date(),+3);
    var timeDomainMode = FIT_TIME_DOMAIN_MODE;// fixed or fit
    var taskTypes = [];
    var taskStatus = [];
    var data = [];
    var height = 100;
    var maxDens = 0;
    var width = document.body.clientWidth - margin.right - margin.left-5;
    var colorRange = ['#c12584','#cb4476','#dc7d5c','#f2c53a','#fce62a','#6bbe43','#239849','#1c8165','#1b687a','#254b7e','#302a83']
    var graphData = [];

    var tickFormat = "%D:%H:%M";

    var updateAngular = function(){
    	// scope = angular.element(document.getElementById("ng-main")).scope();
		// scope.$apply();
    }

    // var x = d3.time.scale().domain([ timeDomainStart, timeDomainEnd ]).range([ 0, width ]).clamp(true);
    // var y = d3.scale.ordinal().domain(taskTypes).rangeRoundBands([ 0, height - margin.top - margin.bottom ], .1);
    // var colors = d3.scale.category20c().domain(data.map(function(d){return d.name}));
    // var colors = d3.scale.ordinal().domain(data.map(function(d){return d.name})).range(colorRange);

    // var xAxis = d3.svg.axis().scale(x).orient("top").tickFormat(d3.time.format(tickFormat)).tickSubdivide(true)
	    // .tickSize(8).tickPadding(8);

    // var yAxis = d3.svg.axis().scale(y).orient("left").tickSize(0);

    var initTimeDomain = function() {
	if (timeDomainMode === FIT_TIME_DOMAIN_MODE) {

	    if (data === undefined || data.length < 1) {
		timeDomainStart = d3.time.day.offset(new Date(), -3);
		timeDomainEnd = d3.time.hour.offset(new Date(), +3);
		return;
	    }
	    timeDomainEnd = d3.max(data.map(function(d){return d3.time.day.offset(d.end, +1)}));
	    timeDomainStart = d3.min(data.map(function(d){return d3.time.day.offset(d.start, -1)}));
	    // console.log(timeDomainStart,timeDomainEnd);
	}
    };

    var initAxis = function() {
		x = d3.time.scale().domain([ timeDomainStart, timeDomainEnd ]).range([ 0, width ]);
		// x = d3.scale.linear().domain([0,20]).range([0,width]);
// 
		y = d3.scale.linear().domain([0,maxDens*1.1]).range([height,0]);
		// colors = d3.scale.category20().domain(data.map(function(d){return d.name}));
		xAxis = d3.svg.axis().scale(x).orient("bottom").tickFormat(d3.time.format(tickFormat)).tickSubdivide(true)
			.tickSize(8).tickPadding(5);
		yAxis = d3.svg.axis().scale(y).orient("right").ticks(2).outerTickSize(0).tickPadding(10);;
    };
    
    function workDensity() {
    	// console.log(data);
		initTimeDomain();
		initAxis();
		var chart = d3.select("div#work-density-chart")
			// .style('color','#000')
			.style("width", (width)+'px')
			.attr("class", "chart");

		svg = chart
			.append("svg")
			.attr("width", width)
			.attr("height", height+margin.top+margin.bottom)
			.append("g")
	        .attr("class", "work-density-chart")
			.attr("transform", "translate(" + 0 + ", " + margin.top + ")");
					// .style('margin-right',margin.right+'px')
		;

		 svg.append("g")
			 .attr("class", "x axis")
			 .attr("transform", "translate(0, " +(height)+ ")")
			 .transition()
			 .call(xAxis);
		
		svg.append("g")
			 .attr("class", "y axis")
			 .attr("transform", "translate(0, " +(0)+ ")")
			 .transition()
			 .call(yAxis);		 

			// for(i=timeDomainStart; i<timeDomainEnd;d3.time.day.offset(i,+2)){
			// 	console.log(i);
			// }
		    // var start = timeDomainStart;
		    // var end = timeDomainEnd;
		    // var wdens = new Array();
		    // while(start < end){
		    //    // console.log(start);           
		    //    wdens.push({date:start,value:Math.random()*20})

		    //    var newDate = start.setDate(start.getDate() + 1);
		    //    start = new Date(newDate);
		      
		    // }
		    // console.log(wdens);	
			// console.log(data.map(function(d){
			// 	return d}
			// 	));
			
			// console.log('transition');
			svg.append("path").attr('class','line').attr("d", line(graphData)).attr('stroke','#eee').attr('fill','none');

		 return workDensity;

    };
	
	var line = d3.svg.line()
	.interpolate("basis")
	// assign the X function to plot our line as we wish
	.x(function(d,i) { 
		return x(d.date); 
	})
	.y(function(d) { 
		return y(d.value); 
	});

	var calculateDatespan = function(start, stop){
		var oneDay = 24*60*60*1000; // hours*minutes*seconds*millisecond
		var diffDays = Math.round(Math.abs((start.getTime() - stop.getTime())/(oneDay)));

		return diffDays;
	}
    workDensity.redraw = function() {
		initTimeDomain();
		initAxis();
// 
		// var barHeight = y.rangeBand();//y.rangeBand()
		// var barHeight = 50;//y.rangeBand()

        var svg = d3.select("div#work-density-chart svg");

        // var ganttChartGroup = svg.select(".gantt-chart");

        // var chart = d3.select("#gantt .bars");

    	var start = timeDomainStart;
	    var end = timeDomainEnd;
	    // var wdens = new Array();
	    // console.log(data);

	    // while(start < end){
	    //    // console.log(start);
	    //    var sum = 0;          
	    //    for(j in data){
	    //    	// console.log(x);
	    //    	if(data[j].end >= start && start>= data[j].start)
	    //    		sum+= data[j].sections.sectionSum/calculateDatespan(data[j].start,data[j].end);
	    //    		// sum++;
	    //    		// console.log(data[j].sections.sectionSum,calculateDatespan(data[j].start,data[j].end));
	    //    }
	    //    if(maxDens<sum){
	    //    		maxDens = sum;
	    //    }

	    //    wdens.push({date:start,value:sum})

	    //    var newDate = start.setDate(start.getDate() + 1);
	    //    start = new Date(newDate);

	      

	    // }
	    svg.select("path.line").interrupt().transition().duration(1000).attr("d", line(graphData)).attr('stroke','#eee').attr('fill','none');
	    svg.select(".x").transition().call(xAxis);
	     svg.select(".y").transition().call(yAxis);
		return workDensity;
    };

    workDensity.margin = function(value) {
	if (!arguments.length)
	    return margin;
	margin = value;
	return workDensity;
    };

    workDensity.timeDomain = function(value) {
	if (!arguments.length)
	    return [ timeDomainStart, timeDomainEnd ];
	timeDomainStart = +value[0], timeDomainEnd = +value[1];
	return workDensity;
    };

    /**
     * @param {string}
     *                vale The value can be "fit" - the domain fits the data or
     *                "fixed" - fixed domain.
     */
     workDensity.timeDomainMode = function(value) {
	if (!arguments.length)
	    return timeDomainMode;
        timeDomainMode = value;
        return workDensity;

    };

    workDensity.data = function(value) {
	if (!arguments.length)
	    return data;
        data = value;
        return workDensity;

    };
   
    workDensity.graphData = function(value) {
	if (!arguments.length)
	    return graphData;
        graphData = value;
        return workDensity;

    };
    workDensity.maxDensity = function(value) {
	if (!arguments.length)
	    return maxDens;
        maxDens = value;
        return workDensity;

    };

    workDensity.width = function(value) {
	if (!arguments.length)
	    return width;
	width = +value;
	return workDensity;
    };

    workDensity.height = function(value) {
	if (!arguments.length)
	    return height;
	height = +value;
	return workDensity;
    };

    workDensity.tickFormat = function(value) {
	if (!arguments.length)
	    return tickFormat;
	tickFormat = value;
	return workDensity;
    };


    
    return workDensity;
};
