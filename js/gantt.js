/**
 * @author Mikael Sundgren
 * @version 0.6
 */

d3.gantt = function() {
    var FIT_TIME_DOMAIN_MODE = "fit";
    var FIXED_TIME_DOMAIN_MODE = "fixed";
    
    var margin = {
	top : 20,
	right : 40,
	bottom : 20,
	left : 40
    };

    var timeDomainStart = d3.time.day.offset(new Date(),-3);
    var timeDomainEnd = d3.time.hour.offset(new Date(),+3);
    var timeDomainMode = FIT_TIME_DOMAIN_MODE;// fixed or fit
    var taskTypes = [];
    var taskStatus = [];
    var data = [];
    var height = document.body.clientHeight - margin.top - margin.bottom-5;
    var width = document.body.clientWidth - margin.right - margin.left-5;
    var colorRange = ['#c12584','#cb4476','#dc7d5c','#f2c53a','#fce62a','#6bbe43','#239849','#1c8165','#1b687a','#254b7e','#302a83']
	// var barHeight = y.rangeBand();//y.rangeBand()
	var barHeight = 35;//y.rangeBand()
	var barMargin = 30;

    var tickFormat = "%D:%H:%M";

    var updateAngular = function(){

    	scope = angular.element(document.getElementById("ng-main")).scope();
    	// scope.workDensity.redraw();
		scope.$apply();
    }
    var keyFunction = function(d) {
	return d.start + d.taskName + d.end;
    };

    var rectTransform = function(d) {
	return "translate(" + x(d.start) + "," + y(d.name) + ")";
    };
    var mouseEventX = function(){
   		var eventX = event.x-d3.select(".bars").node().offsetLeft;
   		// console.log(event.x);
    	return eventX;
    }

    var updateBarInfo = function(bar){
    	// console.log(bar);
    	// d3.select(bar).select('info.date.left')
    	d3.select(bar.parentNode).select('.phase-title').html(function(d){return d.name;});
    	d3.select(bar.parentNode).select('.phase-duration').html(function(d){return calculateDatespan(d.start,d.end)+' dagar'});
    	d3.select(bar.parentNode).select('.time-budget').html(function(d){return d3.sum(d.sections.map(function(section){return section.duration;}))+'h'});

    	d3.select(bar).select('.info.date.left').html(function(d){return d.start.getDate() + '/' + (d.start.getMonth()+1);});
	    d3.select(bar).select('.info.date.right').html(function(d){return d.end.getDate() + '/' + (d.end.getMonth()+1);});
    }

	var dragResizeRight = function(d) {
	      var date = Math.max( x.invert(x(d.start)+20), x.invert(mouseEventX())); 
	      d.end = new Date(date);     
	      
	      updateBarInfo(this.parentNode);

	      d3.select(this.parentNode)
	        .style("width", function(d) {return (x(d.end) - x(d.start))+'px';})
	        .style("left", function(d) {return (x(d.start))+'px';})
	      updateAngular();
	}

	var dragResizeLeft = function(d) {
			// console.log('left');
	      var date = Math.min( x.invert(x(d.end)-20), x.invert(mouseEventX())); 
	      d.start = new Date(date);     
	      
	      updateBarInfo(this.parentNode);

	      d3.select(this.parentNode)
	        .style("width", function(d) {return (x(d.end) - x(d.start))+'px';})
	        .style("left", function(d) {return (x(d.start))+'px';})
	        updateAngular();
	}

	var dragBarXstart = function(d) {
		mouseOffset = event.x - d3.select(".bars").node().offsetLeft-d3.select(this.parentNode).node().offsetLeft;
		// mouseOffset = d3.select(this.parentNode).node().offsetLeft;
		console.log(mouseOffset, event.x,d3.select(".bars").node().offsetLeft,d3.select(this.parentNode).node().offsetLeft);
	}
	var dragBarX = function(d) {
	      updateBarInfo(this.parentNode);
	      d3.select(this.parentNode).node().offsetWidth;
	      // console.log(d3.select(this.parentNode).node().offsetLeft);
	      startCoord = d3.select(this.parentNode).node().offsetLeft;
	      endCoord = d3.select(this.parentNode).node().offsetLeft+d3.select(this.parentNode).node().offsetWidth;
	      
	      d.start = x.invert(startCoord);
	      d.end = x.invert(endCoord);

	      d3.select(this.parentNode)
	        .style("left", function(d) {return (mouseEventX()-mouseOffset)+'px';});
	      updateAngular();
	}

	var dragSectionStart = function(d){
		startWidth = this.parentNode.offsetWidth;
		siblingWidth = this.parentNode.nextSibling.offsetWidth;
	}
	var dragEnd = function(d){
		gantt.redraw();
		scope.workDensity.redraw();
	}
	var dragSectionX = function(d) {
		  parentWidth = this.parentNode.parentNode.offsetWidth;
		  startRatio = startWidth/parentWidth;
		  siblingRatio = siblingWidth/	parentWidth;
		  
		  d3.select(this.parentNode)
		  	.style('width', ((d3.event.x/startWidth)*startRatio)*100+'%');

		  currentWidth = this.parentNode.offsetWidth;
		  currentRatio = currentWidth/parentWidth;
		  difference = startRatio-currentRatio;

		  d3.select(this.parentNode.nextSibling)
		  	.style('width', (siblingRatio+difference)*98+'%');

		  d.duration = Math.round(d3.select(this.parentNode.parentNode).data()[0].sections.sectionSum*currentRatio);
		  d3.select(this.parentNode.nextSibling).data()[0].duration = Math.round(d3.select(this.parentNode.parentNode).data()[0].sections.sectionSum*(siblingRatio+difference));

		  d3.select(this.parentNode).select('.title').html(function(section){return section.name+'('+section.duration+'h)'});
		  d3.select(this.parentNode.nextSibling).select('.title').html(function(section){return section.name+'('+section.duration+'h)'});

	}
	var toggleDragStart = function() {
		this.classList.add("active");

	}
	var toggleDragEnd = function() {
		this.classList.remove("active");
		dragEnd();
	}
    var x = d3.time.scale().domain([ timeDomainStart, timeDomainEnd ]).range([ 0, width ]).clamp(true);
    var y = d3.scale.ordinal().domain(taskTypes).rangeRoundBands([ 0, height - margin.top - margin.bottom ], .1);
    // var colors = d3.scale.category20c().domain(data.map(function(d){return d.name}));
    var colors = d3.scale.ordinal().domain(data.map(function(d){return d.name})).range(colorRange);


	var dragRight = d3.behavior.drag()
	    // .origin(d3.select("div#gantt"))
	    .on("drag", dragResizeRight)
	    .on("dragstart", toggleDragStart)
	    .on("dragend", toggleDragEnd);

	var dragLeft = d3.behavior.drag()
	    // .origin(d3.select("div#gantt"))
	    .on("drag", dragResizeLeft)
	    .on("dragstart", toggleDragStart)
	    .on("dragend", toggleDragEnd);

	var dragBar = d3.behavior.drag()
	    // .origin(d3.select("div#gantt"))
	    .on("drag", dragBarX)
	    .on("dragstart", dragBarXstart)
	    .on("dragend", dragEnd)
	    ;
	var dragSection = d3.behavior.drag()
	    // .origin(d3.select("div#gantt"))
	    .on("drag", dragSectionX)
	    .on("dragstart", dragSectionStart)
	    .on("dragend", dragEnd)
	    ;
    // var xAxis = d3.svg.axis().scale(x).orient("top").tickFormat(d3.time.format(tickFormat)).tickSubdivide(true)
	    // .tickSize(8).tickPadding(8);

    // var yAxis = d3.svg.axis().scale(y).orient("left").tickSize(0);

    var initTimeDomain = function() {
	if (timeDomainMode === FIT_TIME_DOMAIN_MODE) {

	    if (data === undefined || data.length < 1) {
		timeDomainStart = d3.time.day.offset(new Date(), -3);
		timeDomainEnd = d3.time.hour.offset(new Date(), +3);
		return;
	    }
	    timeDomainEnd = d3.max(data.map(function(d){return d3.time.day.offset(d.end, +1)}));
	    timeDomainStart = d3.min(data.map(function(d){return d3.time.day.offset(d.start, -1)}));
	}
    };

    var initAxis = function() {
		x = d3.time.scale().domain([ timeDomainStart, timeDomainEnd ]).range([ 0, width]);
		y = d3.scale.ordinal().domain(data.map(function(d){return d.name})).rangeRoundBands([ 0, height - margin.top - margin.bottom ], .1);
		// colors = d3.scale.category20().domain(data.map(function(d){return d.name}));
		xAxis = d3.svg.axis().scale(x).orient("top").tickFormat(d3.time.format(tickFormat)).tickSubdivide(true)
			.tickSize(8).tickPadding(5);
		yAxis = d3.svg.axis().scale(y).orient("left").tickSize(0);
    };
    
    function gantt() {
		initTimeDomain();
		initAxis();

		var chart = d3.select("div#gantt")
			// .style('color','#000')
			.style("width", (width)+'px')
			.attr("class", "chart");

		svg = chart
			.append("svg")
			.attr("width", width + margin.left+margin.right)
			.attr("height", 40)
			.append("g")
	        .attr("class", "gantt-chart")
			.attr("transform", "translate(" + 0 + ", " + margin.top + ")");
			
		chart.append('div').attr('class','bars')
		// .style('margin-left',margin.left+'px')
		.style('height',height+'px');
		// .style('margin-right',margin.right+'px')
		;

		 svg.append("g")
			 .attr("class", "x axis")
			 .attr("transform", "translate(0, " +15+ ")")
			 .transition()
			 .call(xAxis);
		 
		 return gantt;

    };
    var swap = function(theArray, indexA, indexB) {
    	console.log(theArray[indexA],theArray[indexB]);
    	if(theArray[indexA] != undefined && theArray[indexB] != undefined){
    		
	    	var temp = theArray[indexA];
	    	
	    	theArray[indexA] = theArray[indexB];
	   		theArray[indexB] = temp;    		
    	}

	};

	var calculateDatespan = function(start, stop){
		var oneDay = 24*60*60*1000; // hours*minutes*seconds*millisecond
		var diffDays = Math.round(Math.abs((start.getTime() - stop.getTime())/(oneDay)));

		return diffDays;
	}
    gantt.redraw = function() {
		initTimeDomain();
		initAxis();

        var svg = d3.select("svg");

        var ganttChartGroup = svg.select(".gantt-chart");

        var chart = d3.select("#gantt .bars");

        chart.style('height',(barHeight+barMargin)*data.length+'px')

        var dataPoint = chart.selectAll(".data-point").data(data, function( d) {return data.indexOf(d);});
        
        dataPoint.exit().transition().duration(300).style('height','0px').remove();

        newData = dataPoint.enter().append('div').attr('class','data-point').style("height", function(d) { return barHeight+'px'; }).style("line-height", barHeight+'px');
        dataPoint.on('mouseover',function(){
        	// console.log('over');
        	this.classList.add("active")
        	d3.select('body').node().classList.add("active");

        })
        .on('mouseout',function(){
        	// console.log('out');
        	d3.select('body').node().classList.remove("active");
        	this.classList.remove("active");

        });

        phaseInfo = newData
        	  .append("div")
		      .attr('class','phase-info')
		      // .style('width',(width-margin.right)+'px')
		      // .style('left','-'+((width)-margin.right)+'px')
		      .style('top','-'+(barMargin)+'px')
		       // .style("height", function(d) { return barHeight+'px'; })
		       // .style("line-height", function(d) { return barHeight+'px'; })
		       ;

		//Phase info       
        title = phaseInfo
        	  .append("div")
		      .attr('class','phase-title');
		duration = phaseInfo
        	  .append("div")
		      .attr('class','phase-duration icon-before');
		timeBudget = phaseInfo
        	  .append("div")
		      .attr('class','time-budget icon-before');

		orderUp = phaseInfo
        	  .append("div")
		      .attr('class','phase-order-up lsf control')
		      .html('up')
		      .on('click',function(d){
		      	console.log(data.indexOf(d));
		      	swap(data,data.indexOf(d),data.indexOf(d)-1)
		      	// if(data.indexOf(d) != -1)
		      	// 	data.splice(data.indexOf(d),1);
		      	gantt.redraw();
		      });
		orderDown = phaseInfo
        	  .append("div")
		      .attr('class','phase-order-down lsf control')
		      .html('down')
		      .on('click',function(d){
		      	console.log(data.indexOf(d));
		      	swap(data,data.indexOf(d),data.indexOf(d)+1)
		      	// if(data.indexOf(d) != -1)
		      	// 	data.splice(data.indexOf(d),1);
		      	gantt.redraw();
		      });      

		  //edit control    
	     phaseInfo
		      .append("div")
		      .html('edit')
		      .attr('class','control edit lsf')
		      .on('click',function(d){
		      	angular.element(document.getElementById("ng-main")).scope().editPhase(d,false);

		      });

		 //delete control
	     phaseInfo
		      .append("div")
		      .html('delete')
		      .attr('class','control delete lsf')
		      .on('click',function(d){

		      	if(data.indexOf(d) != -1)
		      		data.splice(data.indexOf(d),1);
		      	gantt.redraw();
		      	updateAngular();

		      });

		dataRect = newData
			 .append("div")
		     .style('background-color', function(d){return colors(d.name)})
		     // .style('background-color', '#aaa')
		     // .style('left', function(d) {return x(d.start)+'px';})
			 .attr('class','main-bar')
			 .style("width", '0px');

		// dataRect.call(dragBar);

		dataRect
			 .transition()
        	 .duration(1000)
			 .style("width", function(d) {return (x(d.end) - x(d.start))+'px';})
        	 .style("left", function(d) {return (x(d.start))+'px';});

        //drag left
		 dataRect
			 .append("div")
		     // .style('background-color', 'rgba(0,0,0,0.3)')
			 .attr('class','drag left')
			 .style("height", function(d) { return barHeight+'px'; })
			 .style("width", '10px')
			 .attr("cursor", "ew-resize")
			 .call(dragLeft);

		//drag right
		 dataRect
			 .append("div")
		     // .style('background-color', 'rgba(0,0,0,0.3)')
		     .attr('class','drag right')
			 .style("height", function(d) { return barHeight+'px'; })
			 .style("width", '10px')
			 .attr("cursor", "ew-resize")
			 .call(dragRight);
		//move control	     
	     dataRect
		      .append("div")
		      .attr('class','control move lsf')
		      .html('etc')
		      // .style("width", '10px')
		      // .style("height", '10px')
		      .call(dragBar);
		 
		 //Date info
	     dataRect
		      .append("div")
		      .attr('class','info date left')
		      .style('line-height',(barHeight-10)+'px')
		      ;
	     dataRect
		      .append("div")
		      .attr('class','info date right')
		      .style('line-height',(barHeight-10)+'px')
		      ;	

		 //add section control
	     dataRect
		      .append("div")
		      .html('<span class="lsf">add</span>')
		      .attr('class','control add-section')
		      .style('line-height',(barHeight-10)+'px')
		      // .attr('data-toggle', 'modal')
		      // .attr('data-target', '#addSection')
		      .on('click',function(d){
		      	// console.log(d.start.getDate()+'/'+d.start.getMonth()+' - '+d.end.getDate()+'/'+d.end.getMonth(),data);
		      		// var name = prompt('Section name');
		      		// var duration = prompt('Duration (hours)')*1;

		      		// d.sections.push({"name":name,"duration":duration});
		      		// gantt.redraw();
		      		angular.element(document.getElementById("ng-main")).scope().editSection(d,null,true);
		      	
		      });	     
	     // dataRect
		    //   .append("div")
		    //   .attr('class','phase-title')
		    //   .html(function(d){return d.name});
		 // dataPoint.each(function(d,i){console.log(data.indexOf(d),i);});
	     // dataPoint.append('div').html(function(d,i){return i})
	     dataPoint.style('top',function(d,i){return (i*(barHeight+barMargin))+'px';})

	     existingBars = dataPoint.selectAll('.main-bar');

	     existingBars.each(function(){updateBarInfo(this)});

	     // updateBarInfo

	     existingBars
			.style("height", function(d) { return (barHeight-10)+'px'; })
			.style('margin-top','5px')
			.style("width", '1px')
			.style("width", function(d) {return (x(d.end) - x(d.start))+'px';})
        	.transition()
        	.duration(1000)
        	.style("left", function(d) {return (x(d.start))+'px';})
        	;

       	// Sections
       	//New
       newSections = existingBars.selectAll('.section')
       .data(function(d){
	        	
	        	return d.sections;
        	});
       
       newSections
       		.enter()
       		.append('div')
       		.attr('class','section')
       	    .style('width', function(section){return (section.duration/d3.select(this.parentNode).data()[0].sections.sectionSum)*100+'%'})
       	    .append('div')
       	    .attr('class','title ')
       	    .on('click',function(d){
		      	// console.log(d.start.getDate()+'/'+d.start.getMonth()+' - '+d.end.getDate()+'/'+d.end.getMonth(),data);
		      		// var name = prompt('Section name');
		      		// var duration = prompt('Duration (hours)')*1;

		      		// d.sections.push({"name":name,"duration":duration});
		      		// gantt.redraw();
		      		// console.log(d);
		      		// console.log(d,d.parent,,this,this.parentNode.parentNode);
		      		angular.element(document.getElementById("ng-main")).scope().editSection(d3.select(this.parentNode.parentNode).datum(),d,false);
		      	
		      })
       	    ;

	       	newSections.select('.title').html(function(section){return ' <span class="">'+section.duration+'h</span>'})

       	newSections
       		.filter(function(d){ if(d3.select(this).select('.section-drag').node() == null && this.nextSibling) return true;})
       		.append('div')
       		.attr('class','control section-drag')
       		.call(dragSection);

       	newSections
       		.exit()
       		.remove();
       	 //existing sections
        existingBars.selectAll('.section')
        	.data(function(d){
	        	d.sections.sectionSum = d3.sum(d.sections.map(function(section){return section.duration;}));
	        	// console.log(d.sections);
	        	return d.sections;
        	})
        	.style('width', function(section){return (section.duration/d3.select(this.parentNode).data()[0].sections.sectionSum)*100+'%'})
        	.style('line-height', (barHeight-8)+'px');

	     dataPoint.selectAll('.drag.left')
			.style("height", function(d) { return barHeight+'px'; });

	     dataPoint.selectAll('.drag.right')
			.style("height", function(d) { return barHeight+'px'; });

		svg.select(".x").transition().call(xAxis);

		// updateAngular();

		return gantt;
    };

    gantt.margin = function(value) {
	if (!arguments.length)
	    return margin;
	margin = value;
	return gantt;
    };

    gantt.timeDomain = function(value) {
	if (!arguments.length)
	    return [ timeDomainStart, timeDomainEnd ];
	timeDomainStart = +value[0], timeDomainEnd = +value[1];
	return gantt;
    };

    /**
     * @param {string}
     *                vale The value can be "fit" - the domain fits the data or
     *                "fixed" - fixed domain.
     */
     gantt.timeDomainMode = function(value) {
	if (!arguments.length)
	    return timeDomainMode;
        timeDomainMode = value;
        return gantt;

    };

    gantt.data = function(value) {
	if (!arguments.length)
	    return data;
        data = value;
        return gantt;

    };

    gantt.taskTypes = function(value) {
	if (!arguments.length)
	    return taskTypes;
	taskTypes = value;
	return gantt;
    };
    
    gantt.taskStatus = function(value) {
	if (!arguments.length)
	    return taskStatus;
	taskStatus = value;
	return gantt;
    };

    gantt.width = function(value) {
	if (!arguments.length)
	    return width;
	width = +value;
	return gantt;
    };

    gantt.height = function(value) {
	if (!arguments.length)
	    return height;
	height = +value;
	return gantt;
    };

    gantt.tickFormat = function(value) {
	if (!arguments.length)
	    return tickFormat;
	tickFormat = value;
	return gantt;
    };


    
    return gantt;
};
